from django.db import models

# Create your models here.
from django.db.models import OneToOneField, Model, IntegerField, CharField, EmailField, DateTimeField, FileField, FloatField, AutoField, ForeignKey,BigIntegerField, CASCADE, BooleanField
from django.utils import timezone


class Table(Model):
    name = CharField(max_length=45, null = True)
    size = IntegerField(null=True)

class Solution(Model):
    table = ForeignKey(Table)
    line = CharField(max_length=200, null = True)



